<?php
/* @var $this ProyectosController */
/* @var $model Proyectos */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proyectos-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'nomproyecto'); ?>
		<?php echo $form->textField($model,'nomproyecto',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'nomproyecto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'obGeneral'); ?>
		<?php echo $form->textField($model,'obGeneral',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'obGeneral'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<table class="templateFrame grid" cellspacing="0">
                <thead class="templateHead">
                    <tr>
                        <td>
                            <?php echo $form->labelEx(Proyectos::model(),'nomproyecto');/* estos son los labels */?>
                        </td>
                        <td>
                            <?php echo $form->labelEx(Proyectos::model(),'obGeneral');?>
                        </td>
                        
                    </tr>
                </thead>
                <!-- Aca es donde se van a mostrar tooodo lo que se clone del textarea -->
                <tbody class="templateTarget">
                <?php 
                         $i = 0;
                         $persons=Proyectos::model()->findAll();//por ahora es vacio pero este por lo general es Person::model()->findAll() con las condiciones que necesites
                         foreach($persons as $person): 
                 ?>
                    <tr class="templateContent">
                        <td>
                            <?php echo $form->textField($person,"[$i]nomproyecto",array('style'=>'width:100px')); ?>
                        </td>
                        <td>
                            <?php echo $form->textField($person,"[$i]obGeneral",array('style'=>'width:100px')); ?>
                        </td>
                        <td>
                           
                        </td>
                        <td>
                            <div class="remove"><?php echo Yii::t("ui","Remove");?>
                             <?php echo CHtml::hiddenField("rowIndex_$i",$i,array("class"=>"rowIndex"));?>
                        </td>
                    </tr>
                    <?php
                         $i++;
                        endforeach;
                    ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4">
                            <div class="add"><?php echo Yii::t("ui","New");//aca puedes colocar un texto un boton o lo que quieras?></div>
                             <!-- Todo lo que hay dentro del textarea es lo que se va a clonar, el {0} se usa para llevar el numero de la fila 
                             que empieza en cero y va aumentando -->
                            <textarea class="template" rows="0" cols="0">
                                <tr class="templateContent">
                                    <td>
                                        <?php echo CHtml::textField("Proyectos[{0}][nomproyecto]",'',array('style'=>"width:100px")); ?>
                                    </td>
                                    <td>
                                        <?php echo CHtml::textField("Proyectos[{0}][obGeneral]",'',array("style"=>"width:100px")); ?>
                                    </td>
                                    <td>
                                      
                                    </td>
                                    <td>
                                        <div class="remove"><?php echo Yii::t("ui","Remove");// tambn puedes colcar texto o cual quier boton ?></div>
                                        <?php echo CHtml::hiddenField("rowIndex_{0}","{0}",array("class"=>"rowIndex"));?><!-- Este campo es el que tiene en cuenta desde donde debe 
                                         empezar a aumentar el numero por defecto es "0" pero aumenta en caso de que ya hayan registros -->
                                    </td>
                                </tr>
                            </textarea>
                        </td>
                    </tr>
                </tfoot>
            </table> 