<?php
/* @var $this ProyectosController */
/* @var $data Proyectos */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('idproyecto')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->idproyecto), array('view', 'id'=>$data->idproyecto)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nomproyecto')); ?>:</b>
	<?php echo CHtml::encode($data->nomproyecto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('obGeneral')); ?>:</b>
	<?php echo CHtml::encode($data->obGeneral); ?>
	<br />


</div>