<?php

class ProyectosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','BatchUpdate'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','create2','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Proyectos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proyectos']))
		{
			$model->attributes=$_POST['Proyectos'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->idproyecto));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
		public function actionCreate2()
	{
		$model=new Proyectos;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proyectos']))
		{
			for ($i=0; $i < 5; $i++) { 
				# code...
			$model->attributes=$_POST['Proyectos'][$i];
			if($model->save()) {
				// $this->redirect(array('view','id'=>$model->idproyecto));
			}
			}
		}

		$this->render('_form2',array(
			'model'=>$model,
		));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Proyectos']))
		{
			$model->attributes=$_POST['Proyectos'];
			if($model->save())
				var_dump($model->attributes);
				// $this->redirect(array('view','id'=>$model->idproyecto));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Proyectos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Proyectos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Proyectos']))
			$model->attributes=$_GET['Proyectos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Proyectos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Proyectos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Proyectos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='proyectos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
		public function actionBatchUpdate()
		{
		    // retrieve items to be updated in a batch mode
		    // assuming each item is of model class 'Item'
		  $model =Proyectos::model()->findall();
//		    $proyectos = $this->getItemsToUpdate();
//var_dump($proyectos);	
		    $proyectos=new Proyectos;
		    // var_dump($proyectos);
		    if(isset($_POST['Proyectos']))
		    {  var_dump($_POST['Proyectos'][1]);
		        $valid=true;
		       // foreach($proyectos as $i=>$proyecto)
		        
		        for($i=1;$i<=5;$i++)
        		{
if($valid) {
	
		        //     if(isset($_POST['Proyectos'][$i]))
		                $proyecto->attributes=$_POST['Proyectos'][$i];
}
		              // $proyecto->save();     
		        }
		        //if($valid)  {

						//echo "son validos";
		        //} 


		        // all items are valid
		            // ...do something here
		    }
		    // displays the view to collect tabular input
		    $this->render('batchUpdate',array('proyectos'=>$proyectos));
		}
	public function getItemsToUpdate() {
        // Create an empty list of records
        $proyectos = array();
 // 
        // Iterate over each item from the submitted form
        if (isset($_POST['Proyectos']) && is_array($_POST['Proyectos'])) {
            foreach ($_POST['Proyectos'] as $proyecto) {
                // If Proyecto id is available, read the record from database 
                if ( array_key_exists('idproyecto', $proyecto) ){
                    $proyectos[] = Proyectos::model()->findByPk($Proyecto['idproyecto']);
                }
                // Otherwise create a new record
                else {
                    $proyectos[] = new Proyectos();
                }
            }
        }
        return $proyectos;
    }
}
