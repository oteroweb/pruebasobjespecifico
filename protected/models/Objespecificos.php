<?php

/**
 * This is the model class for table "objespecificos".
 *
 * The followings are the available columns in table 'objespecificos':
 * @property integer $idobjespecifico
 * @property string $nomobjespecifico
 * @property string $producto
 * @property integer $idproyecto
 *
 * The followings are the available model relations:
 * @property Actividad[] $actividads
 * @property Proyectos $idproyecto0
 */
class Objespecificos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'objespecificos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idproyecto', 'numerical', 'integerOnly'=>true),
			array('nomobjespecifico, producto', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('idobjespecifico, nomobjespecifico, producto, idproyecto', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'actividads' => array(self::HAS_MANY, 'Actividad', 'idobjespecifico'),
			'idproyecto0' => array(self::BELONGS_TO, 'Proyectos', 'idproyecto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idobjespecifico' => 'Idobjespecifico',
			'nomobjespecifico' => 'Nomobjespecifico',
			'producto' => 'Producto',
			'idproyecto' => 'Idproyecto',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idobjespecifico',$this->idobjespecifico);
		$criteria->compare('nomobjespecifico',$this->nomobjespecifico,true);
		$criteria->compare('producto',$this->producto,true);
		$criteria->compare('idproyecto',$this->idproyecto);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Objespecificos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
